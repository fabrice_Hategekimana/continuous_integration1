# Continuous Integration1

Dans cet exercice, vous allez tenter de faire du debbuging avec les outils de CI.
Bien sûr le debbuging se fait sur son ordinateur personnel en général (car c'est bien plus rapide et pratique). La visé de cet exercice est de vous apprendre à consulter le pipeline de CI/CD pourvu par GitLab. 

## Prérequis
- Éditeur de c++ (ou bloque note)
- L'outil git
- Un compte GitLab

## Mise en place

### Fork
Il faut tout d'abord "forker" le projet. Cela veut dire que vous allez avoir une copie de ce projet dans votre propre compte.

### Clone
Il vous suffit maintenant de cloner votre copie du projet sur votre ordinateur. Il suffit d'utiliser git clone.

Vous êtes prêt pour commencer.

### Exercice
Il y a une erreur dans le fichier `hello.cpp`. Votre but est de pouvoir voir l'erreur dans le terminal de lancement du CI de GitLab et de corriger l'erreur pour que le CI se lance sans erreur. Il existe déjà un fichier `.gitlab-ci.yml` qui va exécuter le fichier `hello.cpp` et retourner le résultat obtenu.

## Rendu
Vous devez présenter deux screenshots du terminal du CI:

- L'erreur affiché par le terminal
- Le message de fin quand le processus s'est lancé sans erreur.

